const { expect } = require("@jest/globals");

const Service = require("../src/service");
const Currency = require("../src/currency");
const Errors = require("../src/errors");

describe("Withdrawal tests", () => {
	/** @type {Service} */
	let service;

	beforeEach(() => {
		service = new Service();
	});

	test("Overdraw throws InsufficientFunds error", () => {
		let herbert = service.addCustomer("John Herbert", "583");
		let account = service.addAccount("5834", 12000.00, [herbert.id]);

		expect(() => {
			service.withdraw(account.number, 50000, Currency.Symbols.Cad, [herbert.id]);
		}).toThrow(Errors.InsufficientFunds);
	});

	test("Negative withdrawal throws InvalidWithdraw error", () => {
		let chris = service.addCustomer("Chris Griffin", "823");
		let account = service.addAccount("0233", 500.00, [chris.id]);

		expect(() => {
			service.withdraw(account.number, -20.00, Currency.Symbols.Cad, [chris.id]);
		}).toThrow(Errors.InvalidWithdraw);
	});

	test("Withdrawal from joint account without all holders throws", () => {
		let peter = service.addCustomer("Peter Griffin", "123");
		let lois = service.addCustomer("Lois Griffin", "456");

		let account = service.addAccount("5827", 15000, [peter.id, lois.id]);

		// TODO: Specify thrown error
		expect(() => {
			service.withdraw(account.number, 200, Currency.Symbols.Cad, [peter.id]);
		}).toThrow();
		expect(account.balance).toBe(15000);

		// TODO: Specify thrown error
		expect(() => {
			service.withdraw(account.number, 200, Currency.Symbols.Cad, [lois.id]);
		}).toThrow();
		expect(account.balance).toBe(15000);

		service.withdraw(account.number, 200, Currency.Symbols.Cad, [peter.id, lois.id]);
		expect(account.balance).toBe(14800);
	});
});

describe("Fund transfer tests", () => {
	/** @type {Service} */
	let service;

	beforeEach(() => {
		service = new Service();
	});

	test("Transfer by unauthorized customer throws NoWithdrawAccess", () => {
		let peter = service.addCustomer("Peter Griffin", "123");
		let account_0123 = service.addAccount("0123", 150.00, [peter.id]);

		let lois = service.addCustomer("Lois Griffin", "456");
		let account_0456 = service.addAccount("0456", 65000.00, [lois.id]);

		expect(() => {
			service.transferFunds(account_0456.number, account_0123.number, 23.75, Currency.Symbols.Cad, [peter.id]);
		}).toThrow(Errors.NoWithdrawAccess)
	});

	test("Transfer from account with insufficient balance throws InsufficientFunds", () => {
		let peter = service.addCustomer("Peter Griffin", "123");
		let account_0123 = service.addAccount("0123", 150.00, [peter.id]);

		let lois = service.addCustomer("Lois Griffin", "456");
		let account_0456 = service.addAccount("0456", 65000.00, [lois.id]);

		expect(() => {
			service.transferFunds(account_0123.number, account_0456.number, 250, Currency.Symbols.Cad, [peter.id]);
		}).toThrow(Errors.InsufficientFunds)
	});
});

