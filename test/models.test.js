const { expect } = require("@jest/globals");

const { Account, Customer } = require("../src/models");

describe("Model instantiation", () => {
	test("Customer instantiation", () => {
		let customer = new Customer("Stewie Griffin", "777");

		expect(customer.name).toBe("Stewie Griffin");
		expect(customer.id).toBe("777");
	});

	test("Account instantiation", () => {
		let account = new Account("1234", 100);

		expect(account.number).toBe("1234");
		expect(account.balance).toBe(100.00);
	});

	test("Account instantiation with invalid initial balance", () => {
		expect(() => {
			new Account("1234", 1/0);
		}).toThrow();

		expect(() => {
			new Account("1234", Infinity);
		}).toThrow();
	});
});
