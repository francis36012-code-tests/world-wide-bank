const { expect } = require("@jest/globals");

const Currency = require("../src/currency");

describe("Currency conversions", () => {
	test("Conversion between the same currency returns same amount", () => {
		let amount = 100;

		Object.entries(Currency.Symbols)
			.forEach(([, currency]) => {
				let result = Currency.convert(amount, currency, currency);
				expect(result).toBe(amount);
			});
	});

	test("0 CAD to any currency returns 0", () => {
		let amount = 0;

		Object.entries(Currency.Symbols)
			.forEach(([, currency]) => {
				let result = Currency.convert(amount, Currency.Symbols.Cad, currency);
				expect(result).toBe(amount);
			});
	});

	test("1 CAD to MXN returns 10", () => {
		let amount = 1;

		let result = Currency.convert(amount, Currency.Symbols.Cad, Currency.Symbols.Mxn);
		expect(result).toBe(10);
	});

	test("1 MXN to CAD returns 0.1", () => {
		let amount = 1;

		let result = Currency.convert(amount, Currency.Symbols.Mxn, Currency.Symbols.Cad);
		expect(result).toBe(0.1);
	});

	test("1 CAD to USD returns 0.5", () => {
		let amount = 1;

		let result = Currency.convert(amount, Currency.Symbols.Cad, Currency.Symbols.Usd);
		expect(result).toBe(0.5);
	});

	test("1 USD to CAD returns 2", () => {
		let amount = 1;

		let result = Currency.convert(amount, Currency.Symbols.Usd, Currency.Symbols.Cad);
		expect(result).toBe(2);
	});
});
