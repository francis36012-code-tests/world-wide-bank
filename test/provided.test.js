const { expect } = require("@jest/globals");

const Service = require("../src/service");
const Currency = require("../src/currency");
const Errors = require("../src/errors");

describe("Provided test cases", () => {
	/** @type {Service} */
	let service;

	beforeEach(() => {
		service = new Service();
	});

	test("Case 1", () => {
		let stewie = service.addCustomer("Stewie Griffin", "777");
		let account = service.addAccount("1234", 100.00, [stewie.id]);

		service.deposit(account.number, 300.00, Currency.Symbols.Usd, stewie.id);

		expect(account.balance).toBe(700.00)
	});

	test("Case 2", () => {
		let glenn = service.addCustomer("Glenn Quagmire", "504");
		let account = service.addAccount("2001", 35000.00, [glenn.id]);

		service.withdraw(account.number, 5000.00, Currency.Symbols.Mxn, [glenn.id]);
		service.withdraw(account.number, 12500.00, Currency.Symbols.Usd, [glenn.id]);
		service.deposit(account.number, 300.00, Currency.Symbols.Cad);

		expect(account.balance).toBe(9800.00)
	});

	test("Case 3", () => {
		let joe = service.addCustomer("Joe Swanson", "002");
		let account_1010 = service.addAccount("1010", 7425.00, [joe.id]);
		let account_5500 = service.addAccount("5500", 15000.00, [joe.id]);

		service.withdraw(account_5500.number, 5000.00, Currency.Symbols.Cad, [joe.id]);
		service.transferFunds(account_1010.number, account_5500.number, 7300.00, Currency.Symbols.Cad, [joe.id]);
		service.deposit(account_1010.number, 13726.00, Currency.Symbols.Mxn, joe.id);

		expect(account_1010.balance).toBe(1497.60);
		expect(account_5500.balance).toBe(17300.00);
	});

	test("Case 4", () => {
		let peter = service.addCustomer("Peter Griffin", "123");
		let account_0123 = service.addAccount("0123", 150.00, [peter.id]);

		let lois = service.addCustomer("Lois Griffin", "456");
		let account_0456 = service.addAccount("0456", 65000.00, [lois.id]);

		service.withdraw(account_0123.number, 70.00, Currency.Symbols.Usd, [peter.id]);
		service.deposit(account_0456.number, 23789, Currency.Symbols.Usd, lois.id);
		service.transferFunds(account_0456.number, account_0123.number, 23.75, Currency.Symbols.Cad, [lois.id]);

		expect(account_0123.balance).toBe(33.75);
		expect(account_0456.balance).toBe(112554.25);
	});

	test("Case 5", () => {
		let joe = service.addCustomer("Joe Swanson", "002");
		let account_1010 = service.addAccount("1010", 7425.00, [joe.id]);

		let john = service.addCustomer("John Shark", "219");

		expect(() => {
			service.withdraw(account_1010.number, 100.00, Currency.Symbols.Usd, [john.id]);
		}).toThrow(Errors.NoWithdrawAccess);

		expect(account_1010.balance).toBe(7425.00);
	});
});
