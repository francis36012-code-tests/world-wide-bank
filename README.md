# World Wide Bank
See [challenge document](./docs/spec.docx).

## Running Tests
Tests are located in `./test` and make use of the `jest` library. 
The tests may be executed by running:

```bash
$ npm run test
```
