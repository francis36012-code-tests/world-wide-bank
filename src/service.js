const Assert = require("assert");

const {
	Customer,
	Account,
} = require("./models");

const Errors = require("./errors");
const InMemoryStorage = require("./storage");
const Currency = require("./currency");


class Service {
	constructor() {
		this.storage = new InMemoryStorage();
	}

	/**
	 * Add an account
	 *
	 * @param {string} accountNumber - Account number
	 * @param {number} balance - Account balance
	 * @param {string[]} holders - The holders of the new account
	 */
	addAccount(accountNumber, balance, holderIds) {
		Assert.ok(
			holderIds.every((id) => this.storage.customers.has(id)),
			"All holders must be registered before account is registered"
		);

		Assert.ok(
			holderIds.length >= 1,
			"There must be at least one account holder"
		);

		let account = new Account(accountNumber, balance);
		this.storage.accounts.set(accountNumber, account);

		this.storage.addAccountHolders(accountNumber, holderIds);

		return account;
	}

	/**
	 * Add a customer
	 *
	 * @param {string} name - The customer's name
	 * @param {string} id - The customer's ID
	 */
	addCustomer(name, id) {
		let customer = new Customer(name, id);
		this.storage.customers.set(id, customer);

		return customer;
	}

	/**
	 * Transfer funds between two accounts
	 *
	 * @param {string} srcAccountNumber - The source account's number
	 * @param {string} tgtAccountNumber - The target account's number
	 * @param {number} amount - The transfer amount in CAD
	 * @param {Symbol} currency
	 * @param {string[]} initiator - The customer(s) making the transfer
	 */
	transferFunds(srcAccountNumber, tgtAccountNumber, amount, currency, initiator) {
		let sourceAccount = this.storage.accounts.get(srcAccountNumber);
		Assert.ok(sourceAccount, new Errors.NonExistentAccount(srcAccountNumber));

		let targetAccount = this.storage.accounts.get(tgtAccountNumber);
		Assert.ok(targetAccount, new Errors.NonExistentAccount(tgtAccountNumber));

		if (!this.hasWithdrawAccess(srcAccountNumber, initiator)) {
			let error = new Errors.NoWithdrawAccess(srcAccountNumber, initiator);
			this.notifyHolders(
				srcAccountNumber,
				{
					timestamp: new Date(),
					message: error.message,
					priority: "high",
				}
			);

			throw error;
		}

		let amountCad = Currency.convert(amount, currency, Currency.Symbols.Cad);

		if (sourceAccount.balance < amountCad) {
			throw new Errors.InsufficientFunds(srcAccountNumber, amountCad, sourceAccount.balance);
		}

		targetAccount.balance += amountCad;
		this.addAccountEventLog(
			tgtAccountNumber,
			{
				type: "transfer:receive",
				timestamp: new Date(),
				sub: initiator,
				sourceAccount: srcAccountNumber,
				targetAccount: tgtAccountNumber,
				amount: amount,
				currency: currency,
			}
		);

		sourceAccount.balance -= amountCad;
		this.addAccountEventLog(
			srcAccountNumber,
			{
				type: "transfer:send",
				timestamp: new Date(),
				sub: initiator,
				sourceAccount: srcAccountNumber,
				targetAccount: tgtAccountNumber,
				amount: amount,
				currency: currency,
			}
		);
	}

	/**
	 * Deposit funds into the specified account
	 *
	 * @param {string} accountNumber - The destination account's number
	 * @param {number} amount - The amount being deposited
	 * @param {Symbol} currency - The currency being deposited in
	 * @param {string} initiator - The customer making the deposit
	 */
	deposit(accountNumber, amount, currency, initiator) {
		let account = this.storage.accounts.get(accountNumber);
		Assert.ok(account, new Errors.NonExistentAccount(accountNumber));

		let amountCad = Currency.convert(amount, currency, Currency.Symbols.Cad);
		account.balance += amountCad;
		this.addAccountEventLog(
			accountNumber,
			{
				type: "deposit",
				timestamp: new Date(),
				sub: initiator,
				account: accountNumber,
				amount: amount,
				currency: currency,
			}
		);
	}

	/**
	 * Withdraw funds from the specified account
	 *
	 * @param {string} accountNumber - The source account's number
	 * @param {number} amount - The amount being withdrawn
	 * @param {Symbol} currency - The currency of the withrawal
	 * @param {string[]} - The account holder(s) making the withdrawal
	 */
	withdraw(accountNumber, amount, currency, initiator) {
		let account = this.storage.accounts.get(accountNumber);
		Assert.ok(account, new Errors.NonExistentAccount(accountNumber));

		if (!this.hasWithdrawAccess(accountNumber, initiator)) {
			this.notifyHolders(accountNumber);
			throw new Errors.NoWithdrawAccess(accountNumber, initiator);
		}

		let amountCad = Currency.convert(amount, currency, Currency.Symbols.Cad);

		if (amount <= 0) {
			throw new Errors.InvalidWithdraw(accountNumber, amountCad);
		}

		if (account.balance < amountCad) {
			throw new Errors.InsufficientFunds(accountNumber, amountCad, account.balance);
		}

		account.balance -= amountCad;
		this.addAccountEventLog(
			accountNumber,
			{
				type: "withdraw",
				timestamp: new Date(),
				sub: initiator,
				account: accountNumber,
				amount: amount,
				currency: currency,
			}
		);
	}

	/**
	 * @param {string} accountNumber
	 * @param {AccountEvent} event
	 */
	addAccountEventLog(accountNumber, event) {
		let eventLogs = this.storage.accountEventLogs.get(accountNumber) || [];
		eventLogs.push(event);

		this.storage.accountEventLogs.set(accountNumber, eventLogs);
	}

	/**
	 * @param {string} customerId
	 * @param {CustomerAlert} alert
	 */
	addCustomerAlert(customerId, alert) {
		let alerts = this.storage.customerAlerts.get(customerId) || [];
		alerts.push(alert);

		this.storage.customerAlerts.set(customerId, alerts);
	}

	/**
	 * @param {string} accountNumber
	 * @param {CustomerAlert} alert
	 */
	notifyHolders(accountNumber, alert) {
		let holderIds = this.storage.getAccountHolderIds(accountNumber);

		holderIds.forEach((holderId) => {
			this.addCustomerAlert(holderId, { ...alert, customerId: holderId });
		});
	}

	/**
	 * @param {string} accountNumber
	 * @param {string[]} customerIds
	 */
	hasWithdrawAccess(accountNumber, customerIds) {
		Assert.ok(customerIds.length >= 1, "At least one customer ID must be provided for withdrawal access check");

		let holderIds = this.storage.getAccountHolderIds(accountNumber);

		// if the account is non-joint, then the customer has withdrawal access
		// if the hold the account.
		if (holderIds.size === 1) {
			return holderIds.has(customerIds[0]);
		}

		// NOTE: For now, we check that all account holders are part of the withdrawal request for
		// joint accounts
		return Array.from(holderIds)
			.every((holderId) => customerIds.includes(holderId));
	}
}

module.exports = Service;

