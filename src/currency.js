const Assert = require("assert");

const Symbols = {
	Cad: Symbol("CAD"),
	Mxn: Symbol("MXN"),
	Usd: Symbol("USD"),
};

const ExchangeRates = new Map([
	[
		Symbols.Cad,
		{
			[Symbols.Cad]: 1,
			[Symbols.Mxn]: 10.0,
			[Symbols.Usd]: 0.5,
		}
	]
]);

/**
 * Converts amount from the source currency to the target currency
 *
 * @param {number} amount - The amount to convert
 * @param {Symbol} srcCurrency - The source currency
 * @param {Symbol} dstCurrency - The target currency
 */
function convert(amount, srcCurrency, targetCurrency) {
	Assert.ok(isFinite(amount), "Invalid conversion amount");
	Assert.ok(!!srcCurrency && !!targetCurrency, "Either source/target currency not provided");

	if (srcCurrency === targetCurrency) {
		return amount;
	}

	let cadRates = ExchangeRates.get(Symbols.Cad);

	let cadSrcRate = cadRates[srcCurrency];
	let cadTargetRate = cadRates[targetCurrency];

	let amountCad = amount / cadSrcRate;
	return amountCad * cadTargetRate;
}

module.exports = {
	Symbols,
	convert,
};
