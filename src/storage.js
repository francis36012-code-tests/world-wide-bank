class InMemoryStorage {
	constructor() {
		/**
		 * Account Number -> Account
		 */
		this.accounts = new Map();

		/**
		 * Customer Id -> Customer
		 */
		this.customers = new Map();

		/**
		 * Customer Id -> Set<AccountNumber>
		 */
		this.customerHoldings = new Map();

		/**
		 * Account Number -> AccountEvent[]
		 */
		this.accountEventLogs = new Map();

		/**
		 * Customer Id -> CustomerAlert[]
		 */
		this.customerAlerts = new Map();
	}

	/**
	 * Get IDs of customers that are on the specified account
	 */
	getAccountHolderIds(accountNumber) {
		let holders = new Set([]);

		for (let [customerId, customerAccounts] of this.customerHoldings.entries()) {
			if (!customerAccounts.has(accountNumber)) {
				continue;
			}
			holders.add(customerId);
		}

		return holders;
	}

	/**
	 * Add the customers specified in the `holderIds` array to the account
	 */
	addAccountHolders(accountNumber, holderIds) {
		holderIds.forEach((holderId) => {
			let holdings = this.customerHoldings.get(holderId) || new Set();
			holdings.add(accountNumber);

			this.customerHoldings.set(holderId, holdings);
		});
	}

	/**
	 * Get the accounts the specified customer holds
	 */
	getCustomerAccountNumbers(customerId) {
		return Array.from(this.customerHoldings.get(customerId) || []);
	}
}

module.exports = InMemoryStorage;
