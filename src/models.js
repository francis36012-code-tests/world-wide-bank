const Assert = require("assert");

class Customer {
	/**
	 * Creates a customer instance
	 *
	 * @param {string} name - The customer's name
	 * @param {string} id - The customer's ID
	 */
	constructor(name, id) {
		this.name = name;
		this.id = id;
	}
}

class Account {
	/**
	 * Creates an account instance
	 *
	 * @param {string} number - Account number
	 * @param {number} balance - Account balance
	 */
	constructor(number, balance) {
		Assert.ok(isFinite(balance), `Invalid account balance: ${balance}`);

		this.number = number;
		this.balance = +balance;
	}
}

module.exports = {
	Customer,
	Account,
};
