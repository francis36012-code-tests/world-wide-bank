class InsufficientFunds extends Error {
	/**
	 * @param {string} accountNumber
	 * @param {number} amountCad
	 * @param {number} balanceCad
	 */
	constructor(accountNumber, amountCad, balanceCad) {
		super(`${accountNumber} has less than ${amountCad} CAD in balance, actual balance is ${balanceCad} CAD`);

		this.accountNumber = accountNumber;
		this.amountCad = amountCad;
		this.balanceCad = balanceCad;
	}
}

class InvalidWithdraw extends Error {
	/**
	 * @param {string} accountNumber
	 * @param {number} amountCad
	 */
	constructor(accountNumber, amountCad) {
		super(`Invalid withdraw request on account ${accountNumber}, requested amount is ${amountCad} CAD`);

		this.accountNumber = accountNumber;
		this.amountCad = amountCad;
	}
}

class NoWithdrawAccess extends Error {
	/**
	 * @param {string} accountNumber
	 * @param {string[]} initiator - Customer(s) that initiated the withdraw request
	 */
	constructor(accountNumber, initiator) {
		super(`Withdrawal from ${accountNumber} denied for customers: (${initiator.join(", ")})`);

		this.accountNumber = accountNumber;
		this.initiator = initiator;
	}
}

class NonExistentAccount extends Error {
	constructor(accountNumber) {
		super(`No account exists with account number ${accountNumber}`);

		this.accountNumber = accountNumber;
	}
}

module.exports = {
	InsufficientFunds,
	InvalidWithdraw,
	NoWithdrawAccess,
	NonExistentAccount,
}
